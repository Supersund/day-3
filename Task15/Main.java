import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		Person[] contactList = { new Person("Craig", "Marais", "4324523"), new Person("Mariah", "Craigy", "23423423"),
				new Person("Craigack", "Craigama", "3242"), new Person("Donald", "Craig", "666"),
				new Person("Craigimir", "Putin", "342342") };
		Person[] matchedContacts = Arrays.stream(contactList).filter(contact -> contact.containsSearchPhrase(args[0]))
				.toArray(Person[]::new);
		for (Person person : matchedContacts) {
			System.out.println(person);
		}
	}
}