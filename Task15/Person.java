public class Person {
	private String firstName;
	private String lastName;
	private String tlfNumber;

	public Person() {
		this.firstName = "Unknown";
		this.lastName = "Unknown";
		this.tlfNumber = "Unknown";
	}

	public Person(String firstName, String lastName, String tlfNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.tlfNumber = tlfNumber;
	}

	public boolean containsSearchPhrase(String searchPhrase) {
		return this.firstName.contains(searchPhrase) || this.lastName.contains(searchPhrase);
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setTlfNumber(String tlfNumber) {
		this.tlfNumber = tlfNumber;
	}

	public String getTlfNumber() {
		return this.tlfNumber;
	}

	public String toString() {
		return this.getFirstName() + " " + this.getLastName();
	}

}
