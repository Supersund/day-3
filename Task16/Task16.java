import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Task16 {

    public static void main(String[] args) {
        File file = new File(args[0]);
        FileReader fileReader;
        try {
            String searchWord = args[1];
            fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
            String line;
            int lines = 0;
            int count = 0;
            String[] words;

            while ((line = br.readLine()) != null) {
                lines++;
                words = line.split(" ");
                for (String word : words) {
                    if (word.toLowerCase().equals(searchWord.toLowerCase())) {
                        count++;
                    }
                }
            }
            System.out.println("The name of the file is: " + file.getName());
            System.out.println("Length of file: " + file.length() + " bytes");
            System.out.println("The file contains " + lines + " lines");
            System.out.println("You searched for: " + searchWord + ". It was found " + count + " times.");

        } catch (IOException e) {
            System.out.println("I don't think that file exists, Craig..");
            System.out.println(
                    "You should include the name of the file (including type extension) as the first argument, and the searchphrase as second argument.");
            System.exit(1);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Did you forget to add two arguments, Craig?");
            System.out.println(
                    "You should include the name of the file (including type extension) as the first argument, and the searchphrase as second argument.");
            System.exit(1);
        }

    }
}